angular.module('app', []);

angular.module('app').controller('mainCntrl', ['$scope',
function ($scope) {

  $scope.master = {}; // MASTER DATA STORED BY YEAR
  $scope.selected_year = "positive"
  $scope.years = ["positive", "negative"]
  $scope.selected_threshold = 0
  $scope.value = 0;
  $scope.min = -1;
  $scope.max = 1;
  $scope.step = .1;
  $scope.filters = {};
  $scope.hasFilters = false;
  $scope.tooltip = {};
  // FORMATS USED IN TOOLTIP TEMPLATE IN HTML
  $scope.pFormat = d3.format(".000001%");  // PERCENT FORMAT
  $scope.qFormat = d3.format("0,000000"); // COMMAS FOR LARGE NUMBERS

  $scope.updateTooltip = function (data) {
    $scope.tooltip = data;
    $scope.$apply();
  }

  $scope.addFilter = function (name) {
    $scope.hasFilters = true;
    $scope.filters[name] = {
      name: name,
      hide: true
    };
    $scope.$apply();
  };

  $scope.update = function () {
    var data = $scope.master[$scope.selected_year];

    if (data && $scope.hasFilters) {
      $scope.drawChords(data.filter(function (d) {
        var fl = $scope.filters;
        var v1 = d.importer1, v2 = d.importer2;

        if ((fl[v1] && fl[v1].hide) || (fl[v2] && fl[v2].hide)) {
          return false;
        }
        return true;
      }));
    } else if (data) {
      $scope.drawChords(data);
    }
  };
  $scope.print_threshold_update = function(n,o){
    console.log("in print updates");
    console.log(o);console.log(parseInt(n));
  }
  $scope.to_number = function (s) {return parseInt(s)}

  // IMPORT THE CSV DATA
  d3.csv('./data/lever_press_correlation.csv', function (err, data) {

    data.forEach(function (d) {
      // d.year  = +d.year;
      d.flow1 = +d.flow1;
      d.flow2 = +d.flow2;

      if (!$scope.master[d.year]) {
        $scope.master[d.year] = []; // STORED BY YEAR
      }
      $scope.master[d.year].push(d);
    })
    $scope.update();
  });
  $scope.$watch('selected_year', $scope.update);
  $scope.$watch('filters', $scope.update, true);
  $scope.$watch('selected_threshold', $scope.print_threshold_update, true);
  $scope.$watch('value', function(n,o){
    console.log("in value");
    console.log(n,o);
    var data = $scope.master[$scope.selected_year];
    if (data && $scope.hasFilters) {

      $scope.drawChords(data.filter(function (d) {
          var ret = true;
          var fl = $scope.filters;
          var v1 = d.importer1, v2 = d.importer2;
          var flow = d.flow1
          if ((fl[v1] && fl[v1].hide) || (fl[v2] && fl[v2].hide)) {
            ret = false;
          }else{
          if ($scope.selected_year === "positive"){
            if (flow >= n){
              ret =  true;
            }
            else{ret = false}
          }else{
            if (flow <= n){
              ret =  true;
            }
            else{ret = false}
          }}
          return ret
        }))
    }else if (data){
      $scope.drawChords(data.filter(function (d){
        var ret = true;
        var v1 = d.importer1, v2 = d.importer2;
        var flow = d.flow1
        if ($scope.selected_year === "positive"){
          if (flow >= n){
            ret =  true;
          }
          else{ret = false}
        }else{
          if (flow <= n){
            ret =  true;
          }
          else{ret = false}
        }
        return ret
      }));
    }
  }
    )
}]);
