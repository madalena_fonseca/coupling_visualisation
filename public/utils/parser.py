from scipy.io import loadmat
from itertools import combinations
import numpy as np
import pandas as pd
import csv

f = loadmat('./matrices/fconn_matrix_HF8_20170128_HF5_20170128_HF6_20170128_HF7_20170129_ImgSm.mat' )
f = f["fconn"]
roi_names = [n[0][0] for n in f["roi_names"][0][0]]
corr_roi = f["matrix_per_roi"][0][0]
combs = list(combinations(range(len(roi_names)), 2))
d = {'year': [],
     'importer1': [],'importer2':[],'flow1':[],'flow2':[]}

for comb in combs:
    fl = corr_roi[comb[0], comb[1]]
    if fl > 0:
        d['year'].append("positive")
    else:
        d['year'].append("negative")
    d['flow1'].append(fl)
    d['flow2'].append(fl)
    d['importer1'].append(str(roi_names[comb[0]]))
    d['importer2'].append(str(roi_names[comb[1]]))


e = pd.DataFrame(d)
e = e.sort_values(by ='year')
e.to_csv('./test.csv', index=False, quoting = csv.QUOTE_NONNUMERIC)
